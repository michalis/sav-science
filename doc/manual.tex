\documentclass{article}

\usepackage{calc}
\usepackage{enumitem}
\usepackage[T1]{fontenc}
\usepackage[top=0.8in, bottom=0.8in, left=1in, right=1in]{geometry}
\usepackage{hyperref}
\usepackage{libertine}
\usepackage{multicol}
\usepackage{paralist}
\usepackage{sav-science}

\usepackage{showexpl}
\lstloadlanguages{[LaTeX]Tex}
\lstset{%
    basicstyle=\ttfamily\small,
    commentstyle=\itshape\ttfamily\small,
    showspaces=false,
    showstringspaces=false,
    breaklines=true,
    breakautoindent=true,
    captionpos=t
}

\usepackage[varqu]{zi4}
\usepackage[libertine]{newtxmath}

\newcommand{\bs}{\textbackslash}
\newcommand{\michalis}[1]{{\color{red}{#1}}}

\newcommand{\macro}[1]{\texttt{\bs{}#1}}

\definecomponents[names={l,u}]
\definecomponents[names={ll,uu}, values={Locks, Unlocks}]

\definerelation[name=myrel, color=green!60!black, line=dashed]
\definerelation[name=myrell, value=rf, color=green!60!black, line=dashed]

\definedomains[names={thread,index}]
\definedomains[names={threadd,indexx}, values={thread, index}, instances={t,i}, functions={get\_tid, get\_idx}]
\definedomains[names={threaddd,indexxx}, values={tid,idx}, instances={t,i}]

\definefunkeywords[names={assume,assert}]
\definefunkeywords[names={assumee,assertt}, values={my\_assume,my\_assert}]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{The \textsc{SAV-Science} Package \\
       \small{Manual for version 0.1} }
\author{Michalis Kokologiannakis \\ \small{MPI-SWS}}
\date{}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
\maketitle
\tableofcontents
\newpage


\section{Getting started}
%%%%%%%%%%%%%%%%%%%%%%%%%

This package aims to provide a (non-comprehensive) list of definitions
commonly used in our paper writing. Since many of the macros used are
similar and their definitions become repetitive, this package tries to
provide a systematic and configurable way to define such macros,
especially the ones related to execution graphs.

The package assumes the existence of a handful of packages, including
\texttt{xparse}, \texttt{expl3} and \texttt{tikz}. To obtain all the
required packages in a Debian distribution, please install
\texttt{texlive-latex-recommended}, \texttt{texlive-latex-extra},
\texttt{texlive-science}, and \texttt{texlive-pictures}.

\section{Defining custom macros}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In the following subsections we will see how we can use some
parametric commands to automatically get some families of macros,
mostly relevant to execution graphs. The text that these macros
produce are parameters of these commands, so that we can make
the same macros produce different text, depending on our needs.

\subsection{Graph components}

To define components of an execution graph, we can use the following
command in the preamble:
\begin{equation*}
\macro{definecomponents[names=\{names\_list\} (, values=\{values\_list\})]}
\end{equation*}
The above command does two things: first, it provides math-mode macros that refer
to the specified components of the graph, and second, it provides
macros that refer to labels of the respective types.

For example, writing \macro{definecomponents[names=\{l,u\}]} will
automatically generate the macros \macro{lL}, \macro{llab[2]},
\macro{lU}, and \macro{ulab[2]}. These macros produce
produce $\lL$, $\llab{\#1}{\#2}$, and $\lU$, $\ulab{\#1}{\#2}$, respectively.
However, had we written \macro{definecomponents[names=\{l,u\},
  values=\{Locks,\,Unlocks\}]}, the above macros would have produced
$\lLL$, $\lllab{\#1}{\#2}$, and $\lUU$, $\uulab{\#1}{\#2}$, respectively.

\subsection{Graph relations}

To define relations of an execution graph, we can use the following
command in the preamble:
\begin{equation*}
\macro{definerelation[name=my\_name (, value=my\_value) (, color=my\_color) (, line=my\_style)]}
\end{equation*}
The above command does three things:
\begin{inparaenum}[a)]
\item it provides math-mode macros that refer to this relation in the graph,
\item it defines a \texttt{tikz} style for this relation, and
\item it defines a color for the specified relation so that it
  can be used in other custom definitions.
\end{inparaenum}

For example, writing
\macro{definerelation[name=myrel, color=green!60!black, line=dashed]}
will generate the following macros: \macro{lMYREL}, \macro{lMYRELE}, \macro{lMYRELI} and
\macro{myrel}, that will produce $\lMYREL, \lMYRELE, \lMYRELI$ and $\myrel$,
respectively. In addition, a dashed green line will be printed
whenever the ``myrel'' style is used in \texttt{tikz} drawings, and the color
\texttt{colorMYREL} will also be defined.

Had we written
\macro{definerelation[name=myrel, value=rf, color=green!60!black, line=dashed]},
then the macros \macro{lMYREL}, \macro{lMYRELE}, \macro{lMYRELI} and
\macro{myrel}, would have produced $\lMYRELL, \lMYRELLE, \lMYRELLI$ and $\myrell$,
respectively.

\subsection{Value domains}

To define domains over which values range, we can use the following
command:
\begin{align*}
  \macro{definedomains[names=\{names\_list\} } & \texttt{(, values=\{values\_list\}) } \texttt{(, instances=\{instances\_list\})} \\
                                                    & \texttt{(, functions=\{functions\_list\})]}
\end{align*}
This command will define math-mode macros that refer to the domains specified, macros that
refer to variables of these domains, and macros for functions on these domains.

For example, writing \macro{definedomains[names=\{thread,index\}]}
defines the macros \macro{dTHREAD}, \macro{iTHREAD}, \macro{fTHREAD} and \macro{dINDEX}, \macro{iINDEX}, \macro{fINDEX}
that produce $\dTHREAD,\iTHREAD,\fTHREAD$ and $\dINDEX,\iINDEX,\fINDEX$, respectively. However, if we had
instead written \macro{definedomains[names=\{thread,index\}, instances=\{t,i\}, functions=\{get\bs{}\_tid,get\bs{}\_idx\}]},
the macros \macro{dTHREAD}, \macro{iTHREAD}, \macro{fTHREAD} and \macro{dINDEX}, \macro{iINDEX}, \macro{fINDEX}
would have produced $\dTHREADD,\iTHREADD,\fTHREADD$ and $\dINDEXX,\iINDEXX,\fINDEXX$, respectively.
Finally, had we written
\macro{definedomains[names= \{thread,index\}, values=\{tid, idx\}, instances=\{t,i\}]},
the above macros would have produced $\dTHREADDD,\iTHREADDD,\fTHREADDD$ and $\dINDEXXX,\iINDEXXX,\fINDEXXX$,
respectively.

\subsection{Function keywords}

To define custom function keywords like, \eg assert, we can use the
following command:
\begin{equation*}
\macro{definefunkeywords[names=\{function\_names\} (, values=\{values\_list\})]}
\end{equation*}
This command will define a macro for each one of the provided function
names.  These macros will all take one argument that will be printed
enclosed in parentheses.

For example, writing \macro{definefunkeywords[names=\{assume,assert\}]}
will generate the macros \texttt{assume[1]} and \texttt{assert[1]} that will
produce \assume{\#1} and \assert{\#1}, respectively. Had we written
\macro{definefunkeywords[names= \{assume,assert\}, values=\{my\_assume, my\_assert\}]}
the macros would have produced \assumee{\#1} and \assertt{\#1}, respectively.


\section{Provided definitions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The package also provides a number of useful definitions that are
usually present in most of our papers.

\subsection{Referencing}

The package defines appropriate \texttt{cleveref} macros for enhanced
cross-referencing. The following referencing types are currently
handled:

\begin{multicols}{4}
\begin{description}[itemsep=-1ex, leftmargin=!, labelwidth=\widthof{\ttfamily dstt:}, font={\ttfamily}, labelindent=\parindent]
\item[appendix]
\item[section]
\item[subsection]
\item[figure]
\item[corollary]
\item[lemma]
\item[proposition]
\item[definition]
\item[notation]
\item[theorem]
%% \item[\vspace{\fill}]
%% \item[\vspace{\fill}]
\end{description}
\end{multicols}

In addition, two commands for citing the appendix are provided, namely \macro{citeapp} and
\macro{citeappt}. The former simply boils down to \macro{cite\{appendix\}} while the
later boils down to \macro{cite[\bs{}cref\{\#1\}]\{appendix\}}.
These commands are even more useful in conjunction with the
\texttt{sav-science} default \href{https://gitlab.mpi-sws.org/michalis/wmbib}{bibliography},
that already defines an \texttt{appendix} bib entry, as well as macros
that affect how this entry is displayed.

\subsection{Abbreviations}

The following abbreviation macros are provided:

\begin{multicols}{5}
\begin{description}[itemsep=-1ex, leftmargin=!, labelwidth=\widthof{\ttfamily dstt:}, font={\ttfamily}, labelindent=\parindent]
\item[\bs{}ie:]  \ie
\item[\bs{}eg:]  \eg
\item[\bs{}cf:]  \cf
\item[\bs{}wrt:] \wrt
\item[\bs{}stt:] \stt
\item[\bs{}aka:] \aka
\item[\bs{}resp:] \resp
\end{description}
\end{multicols}

By loading the package with the \texttt{uk} or the \texttt{us} option,
the style of these abbreviations changes accordingly.

\subsection{Notation}

\Cref{tab:notation} shows the math notation provided by the package. Note
that \macro{mathellipsis} is redefined, and that
\macro{defeq} and \macro{eqdef} provide an alternative to
\macro{triangleq}.

\begin{table}[htbp]
\centering
\resizebox{.85\textwidth}{!}{
\begin{tabular}{|l|c|}

\LaTeX & Symbol \\ \hline

\macro{bbN}            & $\bbN$                \\
\macro{bbZ}            & $\bbZ$                \\
\macro{bbQ}            & $\bbQ$                \\
\macro{bbR}            & $\bbR$                \\
\macro{calE}           & $\calE$               \\
\macro{calO}           & $\calO$               \\
\macro{pt[N]}          & $\pt{\#1}{\#2}{\ldots}{\#N}$   \\
\macro{concat}         & $\concat$        \\
\macro{tup[1]}         & $\tup{\#1}$           \\
\macro{sem[1]}         & $\sem{\#1}$           \\
\macro{set[1]}         & $\set{\#1}$           \\
\macro{stm}            & $\stm$                \\
\macro{setcomp[2]}     & $\setcomp{\#1}{\#2}$  \\
\macro{size[1]}        & $\size{\#1}$          \\
\macro{powerset[1]}    & $\powerset{\#1}$      \\
\macro{finpowerset[1]} & $\finpowerset{\#1}$   \\
\end{tabular}
%
\begin{tabular}{|l|c|}

\LaTeX & Symbol \\ \hline

\macro{dom[1]}       & $\dom{\#1}$           \\
\macro{codom[1]}     & $\codom{\#1}$         \\
\macro{before[2]}    & $\before{\#1}{\#2}$   \\
\macro{after[2]}     & $\after{\#1}{\#2}$    \\
\macro{nin}          & $\nin$                \\
\macro{subq}         & $\subq$               \\
\macro{supq}         & $\supq$               \\
\macro{sqsubq}       & $\sqsubq$             \\
\macro{sqsupq}       & $\sqsupq$             \\
\macro{true}         & $\true$   \\
\macro{false}        & $\false$  \\
\macro{mathellipsis} & $\mathellipsis$  \\
\macro{maketil[1]}   & $\maketil{\#1}$  \\
\macro{til}          & $\til$           \\
\macro{cuptil}       & $\cuptil$        \\
\macro{uplustil}     & $\uplustil$      \\
\end{tabular}
%
\begin{tabular}{|l|c|}

\LaTeX & Symbol \\ \hline

\macro{rst[1]}       & $\rst{\#1}$      \\
\macro{imm[1]}       & $\imm{\#1}$      \\
\macro{succof[2]}    & $\succof{\#1}{\#2}$\\
\macro{predof[2]}    & $\predof{\#1}{\#2}$\\
\macro{aite[3]}      & $\aite{\#1}{\#2}{\#3}$ \\
\macro{pfn}          & $\pfn$           \\
\macro{impliess}     & $\impliess$      \\
\macro{iffdef}       & $\iffdef$        \\
\macro{eqdef}        & $\eqdef$         \\
\macro{defeq}        & $\defeq$         \\
\macro{existsa[1]}   & $\existsa{\#1}$  \\
\macro{foralla[1]}   & $\foralla{\#1}$  \\
\macro{inv[1]}       & $\inv{\#1}$      \\
\macro{refC[1]}      & $\refC{\#1}$     \\
\macro{transC[1]}    & $\transC{\#1}$   \\
\macro{reftransC[1]} & $\reftransC{\#1}$\\
\end{tabular}
}
\caption{Math notation}
\label{tab:notation}
\end{table}

\subsection{Array, set, and parentheses enclosure}

To enclose some items in parentheses, a set, or an array, the following macros are provided:
\macro{inpar[1]}, \macro{inset[1]}, and \macro{inarr[N]} (see \cref{tab:enclosure}).
While these macros work only under math mode, they internally use the \texttt{tabular} environment
and can be used to enclose not just plain strings, but also, \eg \texttt{tikz} drawings.

The optional argument in \macro{inarr} can be used to specify a custom delimiter instead
of \texttt{||}.

\begin{table}[H]
\centering
%% \resizebox{.5\textwidth}{!}{
\begin{tabular}{l|c|}

\LaTeX & Produces \\ \hline
\macro{inpar[1]} & $\inpar{\#1}$ \\
\macro{inset[1]} & $\inset{\#1}$ \\
\macro{inarr[N][]} & $\inarr{\#1}{\#2}{\ldots}{\#N}$ \\
\end{tabular}
%% }
\caption{Enclosure macros}
\label{tab:enclosure}
\end{table}

\subsection{Typesetting programs}

\begin{table}[htbp]
\centering
\resizebox{.60\textwidth}{!}{
\begin{tabular}{|l|c|}

\LaTeX & Symbol \\ \hline

\macro{readInst[3][]}    & $\readInst[\#1]{\#2}{\#3}$      \\
\macro{writeInst[3][]}   & $\writeInst[\#1]{\#2}{\#3}$     \\
\macro{assignInst[3][]}  & $\assignInst[\#1]{\#2}{\#3}$    \\
\macro{faiInst[3][]}     & $\faiInst[\#1]{\#2}{\#3}$  \\
\macro{casInst[3][]}     & $\casInst[\#1]{\#2}{\#3}$  \\
\macro{fenceInst[1][]}   & $\fenceInst[\#1]$               \\
\macro{ifInst[1]}        & $\ifInst{\#1}$                  \\
\macro{elseIfInst[1]}    & $\elseIfInst{\#1}$              \\
\macro{elseInst}         & $\elseInst$                     \\
\end{tabular}
%
\begin{tabular}{|l|c|}

\LaTeX & Symbol \\ \hline

\macro{gotoInst[1]} & $\gotoInst{\#1}$   \\
\macro{forInst[1]}  & $\forInst{\#1}$    \\
\macro{doInst}      & $\doInst$          \\
\macro{whileInst}   & $\whileInst{\#1}$  \\
\macro{skipInst}    & $\skipInst$        \\
\macro{breakInst}   & $\breakInst$       \\
\macro{contInst}    & $\contInst$        \\
\macro{assumeInst[1]} & $\assumeInst{\#1}$      \\
\macro{assertInst[1]} & $\assertInst{\#1}$      \\
\end{tabular}
}
\caption{Program keywords and builtins}
\label{tab:instructions}
\end{table}

Programs can be easily typeset using the provided definitions for
instructions (\cref{tab:instructions}), and the \macro{inarr} macro.
For adding annotations to such programs (\eg for the values read or written), one
can use the \macro{readcomment[1]} and \macro{codecomment[1]} macros,
which will print their argument as a C++ comment. For dereferencing
expressions, one can use the \macro{derefExpr[1]} macro, which will
enclose its argument within the delimiters \macro{derefL} and
\macro{derefR}.

Of course, more keywords and builtins can be added, and the style of those
keywords and builtins can be customized: custom keywords (\resp builtins)
can be defined using the \macro{kw[1]} macro (\resp \macro{builtin[1]}),
while the style of the keywords (\resp builtins) can be changed
by redefining the \macro{kwstyle[1]} (\resp \macro{builtinstyle[1]}).
Th assignment symbol can be changed by redefining \macro{assignSymb}.
For \macro{faiInst} and \macro{casInst}, if the first non-optional
argument is empty, the assignment is skipped; the names of these
atomic operations can be adjusted with \macro{kwfai} and
\macro{kwcas}. For \macro{assumeInst} and \macro{assertInst}, an empty
argument does not print parentheses.

\subsection{Algorithmics}

For typesetting algorithms the package provides the algorithmic definitions
\macro{Assert[1]}, \macro{Output[1]}, \macro{Exit[1]}, and
\macro{Continue}, as well as the algorithmic environments
\texttt{switch} (delimited by \macro{Switch[1]} and \macro{EndSwitch}),
\texttt{dowhile} (delimited by \macro{Do} and \macro{doWhile[1]}),
and \texttt{case} (delimited by \macro{Case[1]}, \macro{DefaultCase},
and \macro{EndCase}).

A usage example of these environments is shown below.

{\footnotesize
\begin{LTXexample}[wide,width=.5,preset=\footnotesize,rframe={},%
    codefile=\jobname-\theltxexample-alg.tex]
\begin{algorithm}[H]
\caption{Calculates the days of a month}
\begin{algorithmic}[1]
\Procedure{NumDays}{$y, m$}
\Switch{$m$}
\Case{1,3,5,7,8,10,12}: \Return 31
\EndCase
\Case{4,6,9,11}: \Return 30
\EndCase
\Case{2}:
\If{$(y \mod 4 = 0 \land y \mod 100 \neq 0) \lor
  \phantom{random text} (y \mod 400 = 0)$}
\State \Return 29
\Else{ \Return 28}
\EndIf
\EndCase
\DefaultCase
\Output{Invalid month!}
\Exit{42}
\EndCase
\EndSwitch
\EndProcedure
\end{algorithmic}
\end{algorithm}
\end{LTXexample}
}

\subsection{Access modes}

The package provides the following (math-mode) macros for
the commonly used access modes.

\begin{multicols}{3}
\begin{description}[itemsep=-1ex, leftmargin=!, labelwidth=\widthof{\ttfamily acq:}, font={\ttfamily}, labelindent=\parindent]
\item[\bs{}na:]   $\na$
\item[\bs{}rlx:]  $\rlx$
\item[\bs{}rel:]  $\rel$
\item[\bs{}acq:]  $\acq$
\item[\bs{}acqrel:] $\acqrel$
\item[\bs{}sco:]    $\sco$
\end{description}
\end{multicols}


\newpage
\section{Usage Example}
%%%%%%%%%%%%%%%%%%%%%%%

Finally, an example document that uses the package can be seen below.

{\footnotesize
\begin{LTXexample}[wide,width=.5,preset=\footnotesize,rframe=single,%
    codefile=\jobname-\theltxexample-oota.tex]
\documentclass{article}
\usepackage{sav-science}

\definecomponents[names={e,r,w}]

\definerelation[name=po, value=sb, line=thin]
\definerelation[name=rf, color=green!60!black, line=dashed]
\definerelation[name=co, value=wb, color=orange, line=dotted]

\definedomains[names={val,tid}, instances={v,t}]

\begin{document}

The semantics of a memory model need to somehow forbid
``out-of-thin-air'' values. For example, consider the
following program
\begin{equation*}
\tag{\textsc{lb}}\label{ex:lb}
\inarr{ \readInst{a}{x};\\ \writeInst{y}{a};}{\readInst{b}{y};\\ \writeInst{x}{b};}
\end{equation*}
and suppose we want to check whether $\assertInst{!(x = y = 42)}$
always holds.

Perhaps surprisingly, with no assumptions on the memory model,
this assertion does not hold. And not just for 42, but for any
$\iVAL \in \dVAL$. More specifically, in the offending
executions of the program (\cf the figure below), this
outcome is justified due to the existence of a $\lPO\lRF$
cycle.

{\centering$\inarr{
\begin{tikzpicture}[yscale=.8,xscale=.8]
  \node (0)  at (0,0) {$[init]$};
  \node (11) at (-1,-1) {$\rlab{}{x}$};
  \node (12) at (-1,-2) {$\wlab{}{y, v}$};
  \node (21)  at (1,-1)  {$\rlab{}{y}$};
  \node (22)  at (1,-2)  {$\wlab{}{x, v}$};
  \draw[po] (0) edge (11) edge (21);
  \draw[po] (11) edge node[left] {$\lPO$} (12);
  \draw[po] (21) edge node[right] {$\lPO$} (22);
  \draw[rf] (12) edge node[below] {$\lRF$} (21);
  \draw[rf] (22) edge node[above] {$\lRF$} (11);
  \draw[co, bend right=15] (0) edge node[left] {$\lCO$} (11);
  \draw[co, bend left=15] (0) edge node[right] {$\lCO$} (21);
\end{tikzpicture}}$\par}

A {\color{colorCO}conservative} way to disallow such outcomes
on a language-level memory model would be to forbid
$\lPO\lRFE$ cycles (since $\lRF \defeq \lRFE \cup \lRFI$).
However, more {\color{colorRF}promising} fixes also exist.

\end{document}
\end{LTXexample}
}

\end{document}
